# BlenderVoxelfilter

Small Script to load in blender for fixing inconsistency in Meshes exported from Voxel programs, tested with a Qubicle export, made for Andidev as a small thank you for streaming. Not perfect but works, please give me stuff to test with ^^

# Usage:

Load into Blender, select Mesh to use it on and click run script

Should work in Blender 2.8, not sure about other versions of Blender, propably not earlier versions